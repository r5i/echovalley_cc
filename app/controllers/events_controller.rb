class EventsController < ApplicationController
  protect_from_forgery

  def index
  end

  def venue
  end

  def photo_gallery
  end

  def food_beverage
  end

  def directions
  end

  def contact
  end

end