Paperclip.interpolates('golf_course_name') do |attachment, style|
  attachment.instance.golf_course.name
end

Paperclip.interpolates('golf_hole_course_name') do |attachment, style|
  attachment.instance.golf_hole.golf_course.name
end

Paperclip.interpolates('golf_hole_number') do |attachment, style|
  attachment.instance.golf_hole.hole_number
end
