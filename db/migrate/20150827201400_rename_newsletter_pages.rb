class RenameNewsletterPages < ActiveRecord::Migration
  def self.up
    rename_table :newsletter_pages, :newsletter_campaigns
  end

  def self.down
    rename_table :newsletter_campaigns, :newsletter_pages
  end
end
