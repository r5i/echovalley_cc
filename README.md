# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Through Terminal: cd into your project directory.
* Clone the repo then cd into that repo
* Make sure you're on the latest Ruby version, listed in Gemfile. To make sure run in Terminal `rvm use 2.2.1`
* Create gemset:
    - Open `/config/deploy/demo.rb` copy gemset name from `set :rvm_ruby_version, '2.2.1@evcc'`
    - Confirm that the gemset does not already exist by running: `rvm gemset list`. If the gemset 'evcc' exists skip to the next bullet 
    - In Terminal run `rvm gemset create evcc`
    - Run `rvm gemset list` to confirm that 'evcc' exists.
* Switch to gemset by running `rvm 2.2.1@evcc`
    - Run `rvm gemset list` to confirm that 'evcc' is selected
    - Run `vi .rvmrc`
    - Within this file type `rvm use 2.2.1@evcc`
    - Save file
    - CD out of your project. Then CD into your project. Select yes.
* Install Rails: `gem install rails -v 4.2.1`
* Once Rails is installed, run `bundle install`

### Set Up Database
* Install mysql
* Log into mysql as root user
* In mysql Terminal run ``create user 'echovalley'@'localhost' identified by 'password';``
* In mysql Terminal run ``create database echovalley_dev_db;``
* In mysql Terminal run ``grant all on echovalley_dev_db.* to 'echovalley'@'localhost';``

* Create your database and secret yml fils: in Terminal run `cd /config` `touch database.yml`
```
default: &default
  adapter: mysql2
  encoding: utf8
  pool: 5
  socket: /tmp/mysql.sock
  timeout: 5000

test:
  <<: *default
  username:
  password:
  database: 

development:
  <<: *default
  username: root
  password: PASSWORD
  database: DATABSE_NAME
```
* Create your database and secret yml fils: in Terminal run `cd /config` `touch secrets.yml`
    - In Terminal run `rake secret` to get your secret key
```
development:
  secret_key_base: YOUR_SECRET_KEY
```
* In Terminal run `rake db:migrate`
* In Terminal run `rake db:seed`

### Deploy guidelines ###

* To Deploy to our demo server via terminal
 `cap demo deploy`
 ***(Make sure you're in the root directory of project)***

* To Deploy to our production server via terminal
 `cap prod deploy`
 ***(Make sure you're in the root directory of project)***

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact