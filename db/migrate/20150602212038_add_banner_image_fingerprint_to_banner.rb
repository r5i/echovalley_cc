class AddBannerImageFingerprintToBanner < ActiveRecord::Migration
  def self.up
    add_column :banners, :banner_image_fingerprint, :string
    add_column :banners, :position, :integer
  end

  def self.down
    remove_column :banners, :banner_image_fingerprint
    remove_column :banners, :position
  end
end
