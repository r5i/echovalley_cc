class HolesController < ApplicationController

  def show
    @golf_courses = GolfCourse.all
    @golfcourse = GolfCourse.find_by(slug: params[:golfcourse_slug])
    @hole = GolfHole.find_by(golf_course_id: @golfcourse, hole_number: params[:hole_number])
  end

end