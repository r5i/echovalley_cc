Rails.application.routes.draw do
  get 'members_section/index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  devise_for :admin_members, ActiveAdmin::Devise.config.merge({path: '/members'})
  ActiveAdmin.routes(self)


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  get 'golf',         to: 'golfcourses#golf'
  get 'memberinfo',   to: 'welcome#memberinfo'

  resources :media_files do
     delete 'delete_multiple', on: :collection
  end

  resources :clubs, path: 'club' do
    collection do
      get 'recreation'
      get 'events'
      get 'guest_information'
      get 'community'
      get 'menu'
      get 'dress_code'
      get 'history'
      get 'careers'
      get 'directions'
      get 'contact'
    end  
  end  

  resources :homes, path: 'homes' do
    collection do
      get 'community'
      get 'developments'
      get 'businesses'
      get 'homeshowexpo'
      get 'contact'
    end  
  end  

  resources :events, path: 'events' do
    collection do
      get 'venue'
      get 'photo_gallery'
      get 'food_beverage'
      get 'directions'
      get 'contact'
    end  
  end  

  resources :contacts, only: [:new, :create], path: '/contact', path_names: { new: ''}

  resources :golfcourses, only: [:index, :show], param: :slug do
    resources :holes, param: :hole_number, only: :show
    collection do
      get 'events'
      get 'practice_facilities'
      get 'membership'
      get 'contact'
    end
  end

  resources :membership, only: :index do
    collection do
      get 'events'
      get 'news'
      get 'newsletter'
      get 'directory'
      get 'proshop'
    end
  end

  resources :email_sign_up, path: 'email_sign_up' do
    collection do
      get 'unsubscribe'
      get 'thank_you'
    end  
  end

  get 'course_guide',   to: 'members_section#course_guide'
  get 'directory',   to: 'members_section#directory'
  get 'member_events',   to: 'members_section#member_events'
  get 'member_home',   to: 'members_section#member_home'
  get 'member_news',   to: 'members_section#member_news'
  get 'news_article',   to: 'members_section#news_article'
  get 'pro_shop',   to: 'members_section#pro_shop'
  get 'recent_events',   to: 'members_section#recent_events'
  get 'results',   to: 'members_section#results'

  resources :newsletter_campaigns, only: :show, path: '/newsletters', param: :slug

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
