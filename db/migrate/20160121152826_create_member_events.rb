class CreateMemberEvents < ActiveRecord::Migration
  def change
    create_table :member_events do |t|
      t.string :event_title
      t.text :event_content

      t.timestamps null: false
    end
  end
end
