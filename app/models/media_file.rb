class MediaFile < ActiveRecord::Base

  paginates_per 3

  belongs_to :fileable, polymorphic: true


  has_attached_file :file, :styles => { :large => "620x620>", :medium => "300X240#", :small => "130x130>", :thumb => "100x100#" },
                    :url  => "/system/images/:class/:id/:style/:basename-:fingerprint.:extension",
                    :path => ":rails_root/public/system/images/:class/:id/:style/:basename-:fingerprint.:extension",
                    :use_timestamp => false
  validates_attachment :file, :presence => true,
                       :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png", "application/pdf"] },
                       :size => { :in => 0..2.megabytes }

end