class CreateNewsletterPages < ActiveRecord::Migration
  def self.up
    create_table :newsletter_pages do |t|
      t.string :name, null: false
      t.text :content
      t.string :slug
      t.timestamps
    end
  end

  def self.down
    drop_table :newsletter_pages
  end
end
