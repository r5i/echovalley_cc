class AddGeoLocationsToGolfHoles < ActiveRecord::Migration
  def self.up
    add_column :golf_holes, :black_lat, :decimal, {:precision => 10, :scale => 6}
    add_column :golf_holes, :black_long, :decimal, {:precision => 10, :scale => 6}

    add_column :golf_holes, :white_lat, :decimal, {:precision => 10, :scale => 6}
    add_column :golf_holes, :white_long, :decimal, {:precision => 10, :scale => 6}

    add_column :golf_holes, :blue_lat, :decimal, {:precision => 10, :scale => 6}
    add_column :golf_holes, :blue_long, :decimal, {:precision => 10, :scale => 6}

    add_column :golf_holes, :gold_lat, :decimal, {:precision => 10, :scale => 6}
    add_column :golf_holes, :gold_long, :decimal, {:precision => 10, :scale => 6}

    add_column :golf_holes, :hole_lat, :decimal, {:precision => 10, :scale => 6}
    add_column :golf_holes, :hole_long, :decimal, {:precision => 10, :scale => 6}
  end

  def self.down
    remove_column :golf_holes, :black_lat
    remove_column :golf_holes, :black_long
    remove_column :golf_holes, :white_lat
    remove_column :golf_holes, :white_long
    remove_column :golf_holes, :blue_lat
    remove_column :golf_holes, :blue_long
    remove_column :golf_holes, :gold_lat
    remove_column :golf_holes, :gold_long
    remove_column :golf_holes, :hole_lat
    remove_column :golf_holes, :hole_long
  end
end
