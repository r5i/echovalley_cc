class AddWhatcountsToSiteSettings < ActiveRecord::Migration
  def self.up
    add_column :site_settings, :whatcounts_realm, :string
    add_column :site_settings, :whatcounts_password, :string
  end

  def self.down
    remove_column :site_settings, :whatcounts_realm
    remove_column :site_settings, :whatcounts_password
  end
end
