class CreateGolfCourses < ActiveRecord::Migration
  def self.up
    create_table :golf_courses do |t|
      t.string :name, null: false
      t.text   :course_description
    end
  end

  def self.down
    drop_table :golf_courses
  end
end
