// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap


//= require jquery-fileupload/vendor/jquery.ui.widget
//= require jquery-fileupload/jquery.iframe-transport
//= require jquery-fileupload/jquery.fileupload



$(function () {
      if( $('#mediaFilesUpload').length ) {
          $('#fileupload').fileupload({
              dataType: 'json',
              add: function (e, data) {
                  console.log(data)
                  if (data.files[0].type !== "image/jpeg" && data.files[0].type !== "image/png" && data.files[0].type !== "image/gif" && data.files[0].type !== "application/pdf") {
                      data.context = $('<li/>').addClass('list-group-item').text('File type not accepted: '+data.files[0].name).appendTo(document.body);
                  }
                  else if (data.files[0].size > 2097152 ) {
                      data.context = $('<li/>').addClass('list-group-item').text('File size too large: '+data.files[0].name).appendTo(document.body);
                  }
                  else {
                      data.context = $('<li/>').addClass('list-group-item').text('Uploading... '+data.files[0].name).appendTo(document.body);
                      data.submit();
                  }
              },
              progressall: function (e, data) {
                  var progress = parseInt(data.loaded / data.total * 100, 10);
                  $('#progress .progress-bar').css('width',progress + '%').text(progress + '%');
              },
              done: function (e, data) {
                  data.context.text('Upload finished. '+data.files[0].name);
                  $('#done_uploading').toggle()
              }
          });
      }
});