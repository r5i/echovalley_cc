module ApplicationHelper

  def title(page_title)
    content_for(:title) { page_title }
  end

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = 'Echo Valley Country Club'
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  # def active_if_contains(path)
  #   "active" if request.path.include?(path)
  # end
  # def active_if(path)
  #   "active" if current_page?(path)
  # end

  def active_if(path)
    "active" if current_page?(path)
  end

end
