class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :headline
      t.text :teaser
      t.text :content
      t.boolean :active

      t.timestamps null: false
    end
  end
end
