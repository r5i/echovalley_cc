class HomesController < ApplicationController
  protect_from_forgery

  def index
  end

  def community
  end

  def developments
  end

  def businesses
  end

  def homeshowexpo
  end

  def contact
  end

end