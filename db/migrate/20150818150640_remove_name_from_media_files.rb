class RemoveNameFromMediaFiles < ActiveRecord::Migration
  def self.up
    remove_column :media_files, :name
  end

  def self.down
    add_column :media_files, :name, :string
  end
end
