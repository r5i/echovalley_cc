class GolfCoursePhoto < ActiveRecord::Base

  belongs_to :golf_course

  validates :golf_course_id, presence: true

  has_attached_file :photo, :styles => { :medium => "600x440>", :small => "310x200>", :thumb => "100x100#" },
                    :url  => "/system/images/:class/:golf_course_name/:id/:style/:basename-:fingerprint.:extension",
                    :path => ":rails_root/public/system/images/:class/:golf_course_name/:id/:style/:basename-:fingerprint.:extension",
                    :use_timestamp => false
  validates_attachment :photo, :presence => true,
                       :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..1.megabytes }

end