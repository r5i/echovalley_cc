class AddLayoutToNewsletters < ActiveRecord::Migration
  def self.up
    add_column(:newsletter_campaigns, :layout_name, :string)
    add_column(:newsletter_campaigns, :custom_layout_content, :text)
  end

  def self.down
    remove_column(:newsletter_campaigns, :layout_name)
    remove_column(:newsletter_campaigns, :custom_layout_content)
  end
end
