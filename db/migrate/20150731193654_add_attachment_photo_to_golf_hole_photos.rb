class AddAttachmentPhotoToGolfHolePhotos < ActiveRecord::Migration
  def self.up
    change_table :golf_hole_photos do |t|
      t.attachment :photo
    end
    add_foreign_key :golf_hole_photos, :golf_holes
  end

  def self.down
    remove_attachment :golf_hole_photos, :photo
    remove_foreign_key :golf_hole_photos, :golf_holes
  end
end
