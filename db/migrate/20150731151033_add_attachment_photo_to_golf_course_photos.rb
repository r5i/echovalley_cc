class AddAttachmentPhotoToGolfCoursePhotos < ActiveRecord::Migration
  def self.up
    change_table :golf_course_photos do |t|
      t.attachment :photo
    end
  end

  def self.down
    remove_attachment :golf_course_photos, :photo
  end
end
