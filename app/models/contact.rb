class Contact
  include ActiveModel::Model

  attr_accessor :name, :email, :message, :contact_person

  validates_presence_of :name
  validates :email, presence: true
  validates :contact_person, presence: true
  validates :message, presence: true
  validates_length_of :message, :maximum => 1000
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i }


  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end