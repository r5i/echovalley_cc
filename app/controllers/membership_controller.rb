class MembershipController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_admin_member!

  def index

  end

  def events

  end

  def news

  end

end