ActiveAdmin.register GolfCoursePhoto do

  permit_params :photo, :golf_course_id, :photo_fingerprint, :created_at, :updated_at

  config.batch_actions = false

  menu :parent => "Golf Courses"

  actions :all, :except => :show

  index download_links: false do
    column :golf_course
    column :golf_course_photo do |photo|
      image_tag(photo.photo.url(:thumb))
    end

    actions
  end


  form do |f|
    f.inputs 'Golf Course Photos' do
      f.semantic_errors *f.object.errors.keys
      f.input :golf_course_id, :label => 'Golf Course', :required => true, :as => :select, :collection => GolfCourse.all
      f.input :photo, :label => 'Golf Course Photo:', :required => true, :as => :file, :hint => f.object.photo.present? ? image_tag(f.object.photo.url(:small), id: 'blah') : content_tag(:img, '',{ alt: "no image uploaded yet", id: 'blah'})

    end
    f.actions
  end

  filter :golf_course

end