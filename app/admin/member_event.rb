ActiveAdmin.register MemberEvent do

   permit_params :event_title , :event_content
   
   form do |f|
    f.inputs do
      f.input :event_title
      f.input :event_content, as: :html_editor
    end

    f.actions
  end

end
