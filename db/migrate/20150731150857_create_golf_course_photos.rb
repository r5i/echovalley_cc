class CreateGolfCoursePhotos < ActiveRecord::Migration
  def change
    create_table :golf_course_photos do |t|
      t.integer :golf_course_id, null: false
    end
  end
end
