class CreateGolfHolePhotos < ActiveRecord::Migration
  def change
    create_table :golf_hole_photos do |t|
      t.integer :golf_hole_id, null: false
    end
  end
end
