ActiveAdmin.register Banner do

  permit_params :name, :banner_image, :banner_image_fingerprint, :link_to, :headline, :banner_text, :active, :start_date, :end_date,
                :created_at, :updated_at

  config.sort_order = 'position_asc'
  config.paginate = false
  config.per_page = 10

  sortable

  actions :all, :except => :show

  index download_links: false do
    selectable_column
    column :name, :sortable => :name do |banner|
      link_to banner.name, edit_admin_banner_path(banner)
    end
    column :banner_image do |banner|
      image_tag(banner.banner_image.url(:thumb))
    end
    column :start_date
    column :end_date
    column :active

    actions
  end

  form do |f|
    f.inputs 'Banner Details' do
      f.input :name, :label => 'Banner Name:', :required => true
      f.input :headline, :label => 'Headline:', :required => true
      f.input :banner_text, :label => 'Banner Text', :input_html => { :rows => 5 }
      f.input :link_to, :label => 'Target Link: (http:// must be included)'
      f.input :start_date, :label => 'Start Date:', as: :datepicker, datepicker_options: { min_date: 1.days.ago.to_date }
      f.input :end_date, :label => 'End Date:', as: :datepicker, datepicker_options: { min_date: 1.days.ago.to_date }
      f.input :banner_image, :label => 'Banner Image:', :required => true, :as => :file, :hint => f.object.banner_image.present? ? image_tag(f.object.banner_image.url(:original), id: 'blah') : content_tag(:img, '',{ alt: "no image uploaded yet", id: 'blah'})
      f.input :active, :label => 'Make Active?'
    end
    f.actions
  end

  filter :name
  filter :active, :as => :check_boxes
  filter :start_date
  filter :end_date
  filter :banner_image_file_name
  filter :created_at
  filter :updated_at

  batch_action :activate, priority: 1, :confirm => 'Are you sure?' do |selection|
    Banner.find(selection).each do |activate|
      activate.active!
    end
    redirect_to collection_path, alert: "The selected banners are now active."
  end

  batch_action :deactivate, priority: 2, :confirm => 'Are you sure?' do |selection|
    Banner.find(selection).each do |deactivate|
      deactivate.not_active!
    end
    redirect_to collection_path, alert: "The selected banners are now not active."
  end

end