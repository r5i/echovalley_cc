class EmailSignUpController < ApplicationController
  protect_from_forgery

  def index
  end

  def unsubscribe
  end

  def thank_you
  end

end