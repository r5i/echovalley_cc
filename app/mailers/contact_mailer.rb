class ContactMailer < ActionMailer::Base

  def contact_email(message)
    @message = message
    mail(to: @message.contact_person, from: @message.email, subject: 'Echo Valley - Website Contact Message')
  end
end
