class Banner < ActiveRecord::Base

  has_attached_file :banner_image, :styles => { :medium => "300X300>", :thumb => "100x100>" }, :url => "/system/images/:class/:id/:style/:basename-:fingerprint.:extension",
                    :path => ":rails_root/public/system/images/:class/:id/:style/:basename-:fingerprint.:extension",
                    :use_timestamp => false
  validates_attachment :banner_image, :presence => true,
                       :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..1.megabytes }

  validates :name, :headline,
            presence: true

  scope :active, ->{ where(active: true) }

  def active!
    self.update(active: true)
  end

  def not_active!
    self.update(active: false)
  end

end