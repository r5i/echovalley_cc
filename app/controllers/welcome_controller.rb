class WelcomeController < ApplicationController
  protect_from_forgery

  def index
    @banners = Banner.active
  end


  def memberinfo

  end

  def directory

  end

  def member_home

  end

end