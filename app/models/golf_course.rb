class GolfCourse < ActiveRecord::Base

  has_many :golf_holes
  has_many :golf_course_photos, dependent: :destroy
  has_many :golf_hole_photos, through: :golf_holes
  accepts_nested_attributes_for :golf_holes

  liquid_methods :name

  validates :name, presence: true

end