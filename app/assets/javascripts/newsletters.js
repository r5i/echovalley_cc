$(document).ready(function(){

    if($('#newsletter_campaign_layout_name').val() == "newsletters/custom"){
        $('#newsletter_campaign_custom_layout_content_input').show()
    }

    $('#newsletter_campaign_layout_name').on('change', function() {
        if($(this).val() == "newsletters/custom"){
            $('#newsletter_campaign_custom_layout_content_input').toggle()
        }
        else {
            $('#newsletter_campaign_custom_layout_content_input').hide()
        }
    });
});

function get_template_name() {
    var selectedTemp = $('#data_template :selected').text();
    return selectedTemp;
}


function openWindow(default_template) {
    if( get_template_name() === "default" ) {
        var temp_name = default_template;
    } else {
        var temp_name = get_template_name();
    }
    window.open('/admin/newsletters/preview?template_name='+ temp_name ,null, 'height=600,width=700,status=yes,toolbar=no,menubar=no,location=no');
}

function openWindowTemplate(template) {
    window.open('/admin/newsletters/preview?template_name='+ template ,null, 'height=600,width=700,status=yes,toolbar=no,menubar=no,location=no');
}

