class AddPhotoFingerprintToGolfHolePhoto < ActiveRecord::Migration
  def self.up
    add_column :golf_hole_photos, :photo_fingerprint, :string
  end

  def self.down
    remove_column :golf_hole_photos, :photo_fingerprint
  end
end
