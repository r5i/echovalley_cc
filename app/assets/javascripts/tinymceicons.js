$(function () {
    tinymce.init({
        selector: "textarea.table-editor",
        plugins: [
            "image link code preview table lists charmap print advlist autolink hr anchor pagebreak media contextmenu template paste spellchecker"
        ],
        toolbar: "bold italic | undo redo | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image media insertfile charmap spellchecker | table print | preview | code | insertfile",
        file_browser_callback: function(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                title: "File Browser",
                url: "/media_files",
                width: 800,
                height: 600,
                buttons: [{
                    text: 'Close',
                    onclick: 'close',
                    window : win,
                    input : field_name
                }]
            }, {
                oninsert: function(url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
        }
    });
});
