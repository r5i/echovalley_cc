class CreateCourseStatuses < ActiveRecord::Migration
  def change
    create_table :course_statuses do |t|
      t.text :course_comment

      t.timestamps null: false
    end
  end
end
