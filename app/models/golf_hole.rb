class GolfHole < ActiveRecord::Base

  belongs_to :golf_course
  has_many :golf_hole_photos
  accepts_nested_attributes_for :golf_hole_photos, :allow_destroy => true

  validates :hole_number, presence: true

end