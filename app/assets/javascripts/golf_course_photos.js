
$(function() {


    function readURL(input) {
        console.log(input)
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result).addClass("thumbnail").removeClass("hide");
            }
            reader.readAsDataURL(input.files[0]);
            console.log("File: "+input.files[0])
        }
    }

    $("#golf_course_photo_photo").on("change", function() {
        readURL( this );
    });


});
