ActiveAdmin.register_page 'Newsletter Templates' do

  menu :parent => "Newsletters"

  controller do
    layout 'active_admin'
    def index
      @page_title = "Newsletter Templates"
      @templates = Whatcounts.show_templates(whatcount_params)
    end

    private

    def whatcount_params
      ActionController::Parameters.new(realm: @settings.whatcounts_realm, pass: @settings.whatcounts_password)
    end

  end

  page_action :new, method: :get do
    @page_title = "New Template"
  end

  page_action :create, method: :post do
    response = Whatcounts.create_template(whatcount_params, params[:data])
    if response.include?("FAILURE:")
      redirect_to admin_newsletter_templates_new_path, alert: "#{response}"
    else
      redirect_to admin_newsletter_templates_path, notice: "#{response} ID was created"
    end
  end

  page_action :update, method: :post do
    response = Whatcounts.update_template(whatcount_params, params[:template_name], params[:data])
    if response.include?("FAILURE:")
      redirect_to admin_newsletter_templates_new_path, alert: "#{response}"
    else
      redirect_to admin_newsletter_templates_path, notice: "#{response} ID was updated"
    end
  end

  page_action :edit, method: :get do
    @template = Whatcounts.get_template(whatcount_params, params[:template_id])
    @page_title = "Newsletter Template: '#{@template["Data"]["template_name"]}'"
  end


  action_item :new_template, only: :index do
    link_to "New Template", admin_newsletter_templates_new_path, method: :get
  end

  action_item :cancel, only: :edit do
    link_to "Cancel", admin_newsletter_templates_path
  end


end