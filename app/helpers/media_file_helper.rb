module MediaFileHelper

  def file_size_total
    "Total File Size: #{number_to_human_size(MediaFile.sum(:file_file_size))}"
  end

end