ActiveAdmin.register_page 'Newsletters' do

  controller do
    layout 'active_admin'
    def index
      @page_title = "Newsletter Lists"
      @lists = Whatcounts.show_list(whatcount_params)
    end

    private

    def whatcount_params
      ActionController::Parameters.new(realm: @settings.whatcounts_realm, pass: @settings.whatcounts_password)
    end

  end

  page_action :view_newsletter, method: :get do
    @list = Whatcounts.get_list(whatcount_params, params[:list_id])
    @template = Whatcounts.get_template(whatcount_params, @list["Data"]["template"])
    @page_title = "Newsletter '#{@list["Data"]["list_name"]}'"
  end

  page_action :deliver_test_newsletter, method: :get do
    @list = Whatcounts.get_list(whatcount_params, params[:list_id])
    templates = Whatcounts.show_templates(whatcount_params)
    temp_array = []
    templates["Data"]["folder_id"].each_with_index do |x, i|
      temp_array << [templates["Data"]["template_name"][i], templates["Data"]["template_number"][i].to_i]
    end
    @select_array = temp_array
    @template = Whatcounts.get_template(whatcount_params, @list["Data"]["template"])
    @page_title = "Send Test Newsletter: '#{@list["Data"]["list_name"]}'"
  end

  page_action :send_test, method: :post do
    response = Whatcounts.send_test_newsletter(whatcount_params, params[:list_id], params[:data])
    if response.include?("FAILURE:")
      redirect_to admin_newsletters_deliver_test_newsletter_path(list_id: params[:list_id]), alert: "You must include a valid email"
    else
      redirect_to admin_newsletters_view_newsletter_path(list_id: params[:list_id]), notice: "Your test newsletter email was sent"
    end
  end

  page_action :deliver_campaign, method: :get do
    @list = Whatcounts.get_list(whatcount_params, params[:list_id])
    templates = Whatcounts.show_templates(whatcount_params)
    temp_array = []
    templates["Data"]["folder_id"].each_with_index do |x, i|
      temp_array << [templates["Data"]["template_name"][i], templates["Data"]["template_number"][i].to_i]
    end
    @select_array = temp_array
    @template = Whatcounts.get_template(whatcount_params, @list["Data"]["template"])
    @page_title = "Send Campaign Newsletter: '#{@list["Data"]["list_name"]}'"
  end

  page_action :send_campaign, method: :post do
    response = Whatcounts.send_campaign_for_approval(whatcount_params, params[:list_id], params[:data])
    if response.include?("FAILURE:")
      redirect_to admin_newsletters_deliver_campaign_path(list_id: params[:list_id]), alert: "#{response.to_s}"
    else
      redirect_to admin_newsletters_view_newsletter_path(list_id: params[:list_id]), notice: "#{response.to_s} subscribers will recieve this newsletter after approval"
    end
  end

  page_action :preview, method: :get do
    @preview = Whatcounts.template_preview(whatcount_params, params[:template_name])
    render partial: 'preview', :locals => { :preview => @preview }
  end



  action_item :deliver_test_newsletters, only: :view_newsletter do
    link_to "Deliver Test Email", admin_newsletters_deliver_test_newsletter_path(list_id: params[:list_id]), method: :get
  end

  action_item :deliver_campaigns, only: :view_newsletter do
    link_to "Deliver Campign for Approval", admin_newsletters_deliver_campaign_path(list_id: params[:list_id]), method: :get
  end

  action_item :back_to_lists, only: :deliver_test_newsletter do
    link_to "Go Back To Lists", admin_newsletters_path
  end

  action_item :cancel_deliver, only: :view_newsletter do
    link_to "Cancel", admin_newsletters_path
  end



end