class GolfcoursesController < ApplicationController
  protect_from_forgery

  def index
    @golf_courses = GolfCourse.all
  end

  def show
    @golf_courses = GolfCourse.all
    @golfcourse = GolfCourse.find_by(slug: params[:slug])
    @holes = @golfcourse.golf_holes
  end

  def events
  end

  def membership
  end

  def practice_facilities
  end

  def contact
    @contact = Contact.new
    #@contact = Contact.new(params[:contact])

    if @contact.valid?
      ContactMailer.contact_email(@contact).deliver_now
      redirect_to contact_golfcourses_path, notice: "Your email has been sent. Thank you."
    end
  end

  def golf
    @golf_courses = GolfCourse.all
  end
end