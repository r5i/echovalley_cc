class AddSlugToGolfCourses < ActiveRecord::Migration
  def self.up
    add_column :golf_courses, :slug, :string
  end

  def self.down
    remove_column :golf_courses, :slug
  end
end
