//
// Load Google Fonts
//
WebFontConfig = {
  google: { families: [ 'Lora:700:latin', 'Montserrat:400,700:latin', 'Open+Sans:300,400,700:latin' ] }
};
(function() {
  var wf = document.createElement('script');
  wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
  wf.type = 'text/javascript';
  wf.async = 'true';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(wf, s);
})();



$( document ).ready(function() {
  $('#gMap').echoMap();
  //
  // Plugin for the Photo Gallery page
  //
  $('#photoGallery img').on('click',function(){
    var src = $(this).data('src');
    var img = '<img src="' + src + '" class="img-responsive"/>';
    var galleryModal = $('#lgGalleryView');
    galleryModal.modal();
    galleryModal.on('shown.bs.modal', function(){
        galleryModal.find('.modal-body').html(img);
    });
    galleryModal.on('hidden.bs.modal', function(){
        galleryModal.find('.modal-body').html('');
    });
  });

  if ($('#photoGallery').length) {
    var $container = $('#photoGallery');
    $container.imagesLoaded(function () {
        $container.masonry({
            isAnimated: true,
            itemSelector: '.col-xs-4',
            columnWidth: '.col-xs-4',
            // transitionDuration: 0,
            isResizable: true,
            reload: true
        });
    });
  }
  //
  // Plugin for homepage carousel
  //
  if( $('.owl-carousel').length ) {
    $('.owl-carousel').owlCarousel({
      autoplay: true,
      autoplayHoverPause: true,
      loop:  true,
      margin:  10,
      nav:  false,
      responsiveBaseElement: '#owl-container',
      responsive:{
          0:{
              items:1
          },
          768:{
              items:3
          },
          992:{
              items:4
          }
      }
    });
  }
  //
  // Add Swipe to carousel
  //
  if( $('#holeCarousel').length ) {
    var carousel = document.getElementById("holeCarousel");
    
    Hammer(carousel).on("swipeleft", function() {
      $(carousel).carousel('next');
    });
    Hammer(carousel).on("swiperight", function() {
      $(carousel).carousel('prev');
    });
  }
  //
  // Offcanvase menu
  //
  var snapper = new Snap({
      element: document.getElementById('content'),
      dragger: '#open-left',
      disable: 'right',
      addBodyClasses: true,
      hyperextensible: true,
      resistance: 0.5,
      flickThreshold: 50,
      transitionSpeed: 0.3,
      easing: 'ease',
      maxPosition: 266,
      minPosition: -266,
      tapToClose: true,
      touchToDrag: true,
      slideIntent: 40,
      minDragDistance: 5
  });
  if($('.snap-drawer').length) {

    $('.navbar-nav').clone().appendTo( $('.snap-drawer-left') )
    $('.snap-drawer .navbar-nav .has-children > a').on('click', function(event){
      event.preventDefault();
      var selected = $(this);
      if( selected.next('ul').hasClass('is-hidden') ) {
        //desktop version only
        selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
        selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
      } else {
        selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
      }
    });
    $('.go-back').on('click', function(event){
      event.preventDefault();
      $(this).parent('li').parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
    });

    $('#open-left').on('click', function(){
      if( snapper.state().state=="left" ){
          snapper.close();
      } else {
          snapper.open('left');
      }
    });

  }

});









//
// Plugin for Google maps
//
(function(window, $){

  var Plugin = function(elem, options){
      _this = this;
      this.elem = elem;
      this.$elem = $(elem);
      this.options = options;
      this.metadata = this.$elem.data('pluginoptions');
    };

  Plugin.prototype = {
    defaults: {
      iconMarkers: [
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAlCAMAAACas1IJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACx1BMVEUAAAD//v///v///v///v///v///v///v///v///v///v///////v/////w7/D//v///v/S0dKpqKn//v/Lysv//v+fnp/S0dL//v/NzM3//v/My8zMy8zNzM3//v/Ozc7//v/Ozc7//v/o5+j//v/Pzs/7+vv//v///v/Hxsfs6+z//v/T0tP19PXIx8jY19itrK3BwMHU09Ti4eKvrq/s6+zPzs/NzM3My8zPzs/T0dPT0tPg3+Ds6+zNzc3U1NSEhISdnZ3Ly8t6enrMzMyIiIjMzMyHh4fMzMzW1tZ9fX2Hh4fMzMzW1tZ9fX2Hh4fMzMzW1tZ9fX2Hh4fW1tZ9fX3W1tZ9fX3W1tZ9fX3U09R8fHzm5+apqamGhobR0dG8vbx5eXkzLjEzLjE0LzIaFBhfW12ur64+OjwmHyIzLzIzLjEzLjEzLjEzLjEzLjEvKi05NDdPTE0zLzExLC8zLjEzLjEzLjEzLjEzLjEzLjEzLjH//v/////4+Pj39vf6+fr+/v7Dw8OCg4JcXFxTU1NhYWGDg4Pl5eV2dXY1NTU3Nzc8PDw9PT11dXXl5OXw7/BaWlosLCxERERGRkZFRUUtLS1fX1/W1tbZ2NnKycp0c3QrLCtbW1vNy83T0tPV1NXCwcIyMjJDQ0NCQkIlJSWZmZnMy8yAgIA4ODhBQUEsLSxmZWbOzc7Qz9D49/hHR0c2NjYwMTBJSEnGxcbR0NH08/RQUFA/Pz8xMTE+Pz7DwsNHRkfFxMV5eXktLi1hYGHPzs+2tLYnJyeQkJDY19hlZGUwMDAmJiZPUE/JyMnHxsfY2NhNTU0jJCM5OTmwr7Da2drIx8hJSUkuLi40NDQlJiVLSkupqKnLysv09PTd3N2IhohVVFU7OzuIh4jMysy5ubm2tbbX1tfNzM3W1davr6/X19enp6d2dnaqqqp9fX0AAABGN6o2AAAAfHRSTlMAOoPF5/CEKqtQ60/6+lMi7/MosbkzN4zByOXo8enJz4+WP0PEyQM1/P06a3Fx/nZRzs1PDF6q4vj44qpeC7D8/LCPkW1tSkok+PglC+bmCwLCwwKennt7Vlc7Oy7pLxWbFQcRGBwzgDMcGA1Wcn2GipKgkooJPGNtc3Z5dDSyLAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAH4SURBVCjPY2AAA0YmZhZWFmY2RgYEYOeoqYWAGg52mCAnF0iwrr4BJMzFDRXkqaltbGpuaW1rb2qsreHlAwnyC9TUdnR2dff09nR39fXXThAUAooKAwUnTpo8ZerUqVMmT5s+Y+YsEQYGUaD22ZMmT4WAKXPmzpu/QIyBqaZ24aLFU6CCS7qWLpu5XJxBorZmxUqYylXd3avXrF0nySBVu35iL0Rww5Lujd2bNm/ZKs3AWrtt+w6Yyo3d3Tt37d4qw8BSu76lFyHYvXPP3q2yDHK1NftWwrQDwf4Dyw/KMyjU1h7aCbJ9JVhw5eEjR+cpMijV1NYdOz4FqrL7xMlTC04rMzCo1NSeOTttI1hw47nzFy6eUlVjYFDXqKm9dPnclasbr165dv3GxZuaWqDg0a69dXv3nbv3Ft27e3/LzFk3dSBBqTv/wYKLWx4+erjl4uPTevqwYDcwPPVk/tOjj+cv32JkjIgiE1Mzc4snllbWNrYMKMDO/pmDIwM6cHr+4qUzhqjLjFevXTFE3YCi7hiiHp6vvLwxRH18X/n5Y4gGBL4KCsYQZQh5FYopyBD2KhyLaMSrSCyiUa+isYjGxMZhEY1PSEQTSUpOSU1Lz8jMSk6Ci2Xn5OblFxQWFZfk5+XmZENFS8tyyisqq6qrKivKc8pKgSIAbkDxrZiBkX4AAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAlCAMAAACas1IJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACJVBMVEUAAADm5ubm5ubm5ubm5ubm5ubm5ubm5ubm5ubm5ubm5ubt7e3m5ubu7u7Z2dnm5ubm5ua+vr6ZmZnm5ua3t7fm5uaQkJC+vr7m5ua5ubnm5ua4uLi4uLi5ubnm5ua6urrm5ua6urrm5ubS0tLm5ua6urrk5OTm5ubm5ua0tLTV1dXm5ua+vr7e3t61tbXDw8OcnJyurq6/v7/MzMyenp7V1dW6urq4uLi3t7e7u7u9vb29vb3KysrV1dXHx8fT09OCgoKXl5fPz89+fn7MzMyIiIjMzMyHh4fMzMzW1tZ9fX2Hh4fMzMzW1tZ9fX2Hh4fMzMzW1tZ9fX2Hh4fW1tZ9fX3W1tZ9fX3W1tZ9fX3U09R8fHzm5+apqamGhobR0dG8vbx5eXkzLjEzLjE0LzIaFBhfW12ur64+OjwmHyIzLzIzLjEzLjEzLjEzLjEzLjEvKi05NDdPTE0zLzExLC8zLjEzLjEzLjEzLjEzLjEzLjEzLjHm5ubl5eXk5OTn5+fu7u729vb8/Pz9/f37+/vp6en4+Pj////a2tro6Ojc3Ny2tra3t7f5+fn19fXIyMi0tLTBwcHy8vLOzs7Ly8u/v7+4uLj39/fx8fHNzc3GxsbMzMzKysq5ubn+/v7z8/PJycnHx8fw8PDPz8/AwMD6+vrj4+Pr6+u8vLzi4uLd3d21tbW+vr67u7uzs7OsrKzZ2dmpqal4eHjW1taqqqp9fX0AAAA0IndVAAAAfHRSTlMAOoPF5/CEKqtQ60/6+lMi7/MosbkzN4zByOXo8enJz4+WP0PEyQM1/P06a3Fx/nZRzs1PDF6q4vj44qpeC7D8/LCPkW1tSkok+PglC+bmCwLCwwKennt7Vlc7Oy7pLxWbFQcRGBwzgDMcGA1Wcn2GipKgkooJPGNtc3Z5dDSyLAAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAHPSURBVCjPY2AAA0YmZhZWFmY2RgYEYOeoqakFgZoaDnaYICdXTW1dTX19fU1dbQ0XN1SQB8hraGxqbmlsAMry8oEE+QVq6lrb2iGgrbW1Q1AIKCpcU9fZ1A4HXd09IgwMokB7ehGCff0TuieKMTDV1DUgBCdNnjK1e5o4g0RtzXS44IyZ/TNnTeuRZJCqrYebOmlm/+yZc+b2SDOw1tbPgwrOn7xgNlhUhoEFrhakEiIqyyBXW9MGVblwNkh04bQeeQaFmrpFYMHFC0CCsxcv6e5RZFCqqatZCnJSP1hw8oJpPROUGRhUapYtb2+EaJ+9eMqK7m5VNQYGdY2a1pUz58wEmTl5wYqeHk0tUPBot66atnoi0KWz+yfO7e7p0YEEpe6aad3dc9euXQsUW62nDwt2A0OgEiDq6V5nZIyIIhNTM3OLaZZW1ja2DCjAzn69gyMDOnDasHGTM4aoy+YtW10xRN2Aou4Yoh6eW7y8MUR9fLf4+WOIBgRuCQrGEGUI2RKKKcgQtiUci2jElkgsolFborGIxsTGYRGNT0hEE0lKTklNS8/IzEpOgotl5+Tm5RcUFhWX5Ofl5mRDRUvLcsorKquqqyorynPKSoEiABsE2M5klTbQAAAAAElFTkSuQmCC',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAlCAYAAACtbaI7AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAFAElEQVRIx62WSWxcRRqAv6p6z73YbreX2HGDQ2KwSKIsSpwAQgQJDiAkDEkkJjPnHLkQTuQwc5jjSAMcEIecRnMYCYJJWBQkBAIOhEWYBGaCISbOJO4sXknv/bpf1T+H7iYdx05HGv+XelX6/+/9a6kUq4g4eRR4BngSGAZiQAmYBj4DTimtTnM3Ik62ipPj4uTO4kTqeltbAfeLk/nlwFJgJV8OpVyxK4HnReT5Zo5qBgJvAz4KROCD/8zxzg+znElnyQeWnrjP9sFODu7sZ2xbP0oBAiiqmRuZPyW7k+O/Q8XJMDABJFEwvVDipROTfPDDLCiFNhpV/5GzDkQY2zHA6wc3M9wXJ5PJcvnSpfzk5OTeQ3889HMD+h7wHAouLpQYOzbBuYsZOrqjGFVzpjk0K0L+RsDOkXX84+AGEsEci/kKSvHx3of2Pq3EySjwHQqcCGPHznDquyt0dMfRatX0I9ojNzvPo+sD3jgwQqTNBz/Kt99884gG9jcU3//3PKfOXqe9O4a5AxDjoYIcMTvP6ck5PrmQIeobSsUio6OjL2jgceoJP372OngaT6lbQr4leOMhpSx2cQbtLLT5fPrLIplSiGcU1Wr1MQ08AFCqWs5eyWI8vQqw5iHlPO63K+AsaIM2ivPzRYoVi1YKEdnoAe0AVSvkArt6Ho2HlLO4pTTYELSpHSsolEOsk0Z/xnR99PCNJhH1cKsBS7nbgAAOiLUZtK6lUClV1sBlgJiveXBdOzYUbnHW+PWQbwcCWCts6o0R8w1OBKXUVQ2cbtRgbFs/VO1Nb42HlFf28HcJLY8/0E0y5hM6AfhKAycblTm0e4CHR3rIZQLQ9aIszSC2uiKwkKuwdaiLZ7asI3RCpVollUqNa6XVF8CXCMR8w5svbGWwP0F2boFwceY2DxW1cS1kA5JdEf7y1P0MdLSRKQZEo9Fve3t7P9d13SOARWD3UIJ//mETw+05ipkihSqETnBSW/OBpZivMDKU4O8HNvPQfV0sFipY59zQ0NARv823zbfUiyjeKBQKLKUv8t+FHCd/WuTz80tMLxbJVywdbYbh3jhPjPRwYHs/qUSUhUIFpSAajb68fcf21xrR3MxRvnBkenr61XIQkIgYfKO5mgm4Ua5StYJvFMmoTyoZIbTCYr6MUkqSXV2vbN6y+W/NKbpFxt8ZP9DX1/fnaCy2yzeaiKfxtEbrWi6r1lGuWoJKSCLR+X0ikfjrhvs2vLdsmG+XE++e6Ni3b9/BmZmZZ51zj4hIv1IqorUOPOPNofi6s7Pzw46OjvG+dX2FFW6IO8vExMTJSCTy7NTUlDl37pwdHBz88PDhw/vvZOO1gjrnLhpjTLlcJpvNmmKxeKmVTUuoUmpaRDDGEIlEaG9v/7WVjW6lAFxo3ojIhVYGdwM9DwT17wCYWgvoNWCp/r0EXF0LaAlo5PFCff//Qffs2eOaoFNHjx51rWzuxtOGh83rmkAbxWnZTncNddZdARCR9JpBe/p6ciIyF4/H82sGnZ+bv9bb1/t+Lpdr2U4AZqXD0R27IqmBwcQ961NJrCTnZmfv2XT/8ODxf701G9Ge2bhxUzw1sN5PDQzKtdnrdrm9WgbrpPYkv5faIyMWhqEXj8ej/ev7E+mZdNZZV9Zah9T6tQCkgY8mfjyTa3CWXyghNyemE4h7ntdWqVRM+nLaAFZrbYEKUARydf2wGfI/gWKF56iwdpsAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAlCAYAAACtbaI7AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAEvklEQVRIx62WS2xUVRjHf+fcO+/OdGhraYdS2wIJkkCCpYQQwAQTjIkP0BA0ccfSuMCVLNzozoUmhiVbn1AeBjW4UIwRhVhMTAjEtlPaDpQ+oNNO53nvPZ+LmSlDaZkm9p/c3HOT8/3O//vud06OYgWJkb3Ay8BBoAcIAXkgCfwC/KC0uspqJEa2iZEzYkTEyPIyi88ZMbKtHvCwGJleCjNOTkwxI8YpLAefFpHXazmqFgh8C/hQgBjyd85RGDpLaeYaUsqgg834mnYS3HyUUNcRUBoEUDhz6bm34+vi/YtQMdIDDABxFHhzg6R/e4/88GXQ5dhHiwMGQpsO0XjgFHZsC/Nzc4yOji3cunWr79hbx25XoReB11Dgzg/y4NIhShN3sBpqc6mtE3gLEOncjOz5mrF0E15+BpT6qW9330tKjPQCf1VTfvD9i2RvX8GOrgCsyGfBzBTcDz1PZO9pbH+QkF+4du36Hg0crk7Mj5wjN7Q64GwWxhZg4c4N3PuXUVaQbK5Ab2/vUQ0cKLuEwtAXaGtloAJ8dhmYnATXgO2D4vhlTGkWpW0cx9mngc0A4uYpTQ+AtbJD24K5LIxMlYGWBjQ46duIm0MpjYh02UAEAONgnMxTU05XgR7YusZ+aR7Eq04NacpbD7QP7W9cMeV0DpJTUHIrDms6ATtCNUWlVEEDYwDKDuGLbwXvcahtw1yuXEPHLZfgMRmwGzeh7BAiBqXUPQ1crVoKdr+JcZ9MOTlZSXmZeosHgcRBdCBOJfgPDVxAKsXY8g6Bju2YhQqw4rDkLEm5ajIP/tYeAp2vIuLhOCUSiUS/Vlr9CvyOlEuw7oXTBJsbeTgDI5PgrODQy4IOB4n1fYwVasPNpwkGg9ebm5uvVNc/AXgI+Fp3Y+37irumi0IGLK9cN6T8FqfqsJv4vs/xte7FK8xgjGc2btx4wuf3ebWn1LsoTmWzWYZGZ8nPDuOO9VNM/YybGURKJZTfjx3dgr/jIKGeo1jhDtz8DEopgsHg+9t3bP+s2jGLyi5kTySTyU+LxQL4YmjLj5dNIcU0YhyU9qECcaxIBxiXUm4GpZQ0NsY/2Prc1k9q2/Ax9Z/tP9LS0vJhKBTcqbUPZQdB2SilAUGMg3HyuG6RaDR2IxaLfdT5bOfFpb39hM6fO9+wf//+N8bHx18xxuwRkValVEBrXbQtewrFn9Fo9FJDQ0N/yzMt2eU2zFM1MDBwIRAIvDI4OGjdvHnTa29vv3T8+PHDT4ux60GNMSOWZVmFQoH5+Xkrl8uN1oupC1VKJUUEy7IIBAJEIpGhejG63gRguPZDRIbrBawG+i9QrIyLwOBaQCeAh5XxQ+DeWkDzQLWOw1TP3/8D3bVrl6mBDp48edKshdOqw9r3mkCrP6duO60aajxzF0BEUmsGbWppyojIVDgcXlgz6PTU9ERzS/N3mUymbjvBCleH3h07A4n17bENbYk4nsSnJic3dG/qaT/z5TeTAW1bXV3d4cT6Nl9ifbtMTN73lsarJbAo5St5B+VLRsh1XTscDgdb21pjqfHUvPFMQWvtUu7XLJACfhz45+/Fm8jSA8Xl0Y6JAmHbtv2lUslKjaUswNNae0AJyAGZyny3FvIf7QlR4TLxE+sAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAAAyCAMAAADPyLPHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAflBMVEUAAAD/fwD/fwD/fwD/fwD/fwD/fwD/fwD/fwD/fwDMZgDMZgDMZgDMZgDMZgDMZgDMZgDMZgDMZgAzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjEzLjH////MzMz/fwDMZgAzLjEAAABnHX13AAAAJHRSTlMAk0gI9bVsJNtJ97hsJNuTSgq3BztulLLJ3eyI8YmEs8rt9ssxRfhQAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAMRJREFUSMftzscWgyAQRmHSe7WSEI0Gje//hCm4UAIzg9nyr1jwnTuMtZNSVlXFnPefG40ng1xdT2fzxRD32dIh23W1Q1ZzKrvCswZHypodnrU7OAs6IIs6S5biTFmq07PKPSlbb7Y7Z7c/HE+/dyKhcydEdXqI4kwh1FlCoANCdgeHzA4PGRwppDlyqOccQj3niLzzzjvvoAVhFCcpV+7C0ySOwgBnV9F8p5x6N+KGuqx1vYmMdmd+50X5/l8W/JGT7nwBmwd3ZtAqVbYAAAAASUVORK5CYII=',
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAXCAYAAAAC9s/ZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NzZBNDRGNzk2NkJEMTFFNThDRTM4NTg4N0UwQjQ4NzAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NzZBNDRGN0E2NkJEMTFFNThDRTM4NTg4N0UwQjQ4NzAiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo3NkE0NEY3NzY2QkQxMUU1OENFMzg1ODg3RTBCNDg3MCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3NkE0NEY3ODY2QkQxMUU1OENFMzg1ODg3RTBCNDg3MCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgeTUqsAAANxSURBVHjajJRdTJNnFMd/79u3pVAo6EqlpSJgY2odbNGZLSPMuI9gZvaFqIkmGnW7WZZduOzGmN0smdmujMnutmQXLllmtmXsQ0W4EGOWqTWbkInaKVqQJnwISAsF2mfn7fsW3YBkJzlpz3nO+T/nf855Xo0lRSNYQtTnpBqUNjZHciDNtaUj/2O+G+bDAzXs3+hlg27IsRL3fI6eKeInE3z92U0+Fc/0IoByQwuda+bs5pCKkhHHrKhDB90CwCm/briRJNHSzfa7M/QsADg0zdO7lRuRoKrmgThckiMAiSkrIFQKhtsGLYeRMdL1Hax9OE/SMAGORfg8ElLValQSJDA2AJ1X4aHNwJOFLU9B01oxJsBXSclXjZzccZWXNa+hBUZb1H3DZVG53A/t0q6mw4dofL0NTXPQ1/krXZ8cpyUMzVEJylqxNWdoMF6qZJtRItYMDEtrrvTCruMf0/De0YXWBppeofbJRn7ee5B1fli1QpyS0xbgLb2hTD2NwxwWDN6D2mer/pVckLodB4i0REnceeSLlBI1J+Uxic5Jo/U0BMP1LCeB2jBOc4A5y3ZoFOnxlNab5yTO0lWQid9aFiAdv4636pE9MM3fesewOp1JyZiFQoVfI5ccJv7RwUXJ/SeOkLl5i0CNvTqyK98nac9b327it511PDc+Kbfobu4OzVDe/Bq+F9+QOh0MXzjD6LlTRIMufE7ptgf+GqR/Qzd1eYDNFbx6aSu/TKYtejOGh5EH02QyufwiuWXEq/0eykhZI5Rlau3i7R+SfLmwyqc2cqWtnk2D4/mNzQMpw8kTRVJEbs7aKGUl9w1yb/151ph5egHg8HUOzQuvCmd+JZiVYJWdw5G1k00xLJB9f/BOIW8BIDHNnx/0cMIj/JSyKp3XdNkPO8S+/Ysevrs8Qccyzxm6nyfR7CcUl513GzpB4aNnhZBXHtEEqcrT+M2JLqqgILtjbE/Jq6uSxGyBd5F1tu0ibz6evCTAUIZre2K8XypJLjnNM5Dbj1ziWGySTv6vHA3zjWrVldqnqx+foWu5uEIPzCKDohWiK+3C9dgWvb3eS/GKn3KtYo/aceO23hdNFQDMxrxgv3SfaK3oyEqXVmcyGZpVfWJXmutvshQtFv1d9PbjU/DYycW2lpk32NWY/6fsj6mpY9a3Cf4RYAD/ixwRkElTxQAAAABJRU5ErkJggg=='
      ],
      zoom: 18,
      centerLoc: [41.50996722659143, -93.66168200969696],
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      mapTypeControl: false,
      rotateControl: false,
      scrollwheel: false,
      streetViewControl: false,

      featureStyle: {
        fillColor: '#FFFFFF',
        fillOpacity: .1,
        strokeColor: '#FFFFFF',
        strokeOpacity: .2,
        strokeWeight: 3
      }
    },
    init: function() {
      this.config = $.extend({}, this.defaults, this.options, this.metadata);
      google.maps.event.addDomListener(window, 'load', _this.initialize);
      return this;
    },
    initialize: function() {
      var marker,i;
      var cMarkers = [];
      var nMarkers = [];
      var bounds = new google.maps.LatLngBounds();
      var infowindow = new google.maps.InfoWindow();
      var map = new google.maps.Map(document.getElementById('gMap'), _this.config);
      map.setCenter(new google.maps.LatLng(_this.config.centerLoc[0], _this.config.centerLoc[1]));

      map.controls[google.maps.ControlPosition.TOP_RIGHT].push(FullScreenControl(map));
      map.data.loadGeoJson('/map-test.geojson');
      map.data.setStyle(_this.config.featureStyle);

      // Add hole numbers to map
      $.each(_this.config.wholesCenterLoc, function(index, val) { 
        var pin = new google.maps.Marker({
            position: new google.maps.LatLng(val[1], val[2]),
            icon: {
              url: _this.config.iconMarkers[5]
            },
            map: map,
            visible: true
        });
        google.maps.event.addListener(pin, 'click', (function(pin, index) {
          return function() {
            infowindow.setContent(val[0]);
            infowindow.open(map, pin);
          }
        })(pin, i));
        nMarkers.push(pin); // save all markers
      })

      // Add markers to map
      var toggleMarker = (map.getZoom() >= 17)? true : false;
      if( courseMarkers.length ) {
        for (i = 0; i < courseMarkers.length; i++) {
          var markerLatLng = new google.maps.LatLng(courseMarkers[i][1], courseMarkers[i][2]);
          marker = new google.maps.Marker({
            position: markerLatLng,
            icon: {
              url: _this.config.iconMarkers[(courseMarkers[i][3])]
            },
            map: map,
            visible: toggleMarker
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(courseMarkers[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
          cMarkers.push(marker); // save all markers
          bounds.extend(markerLatLng);
        }
      }

      // Click for fullscreen on mobile
      $('#gmapFullscreen').on('click', function(e) {
        e.preventDefault();
        var that = $(this);
        if (that.text() == 'View Map') {
          that.text('Exit Map');
        } else { 
          that.text('View Map');
        }
        $('#gmapResponsive').toggleClass('overlay');
        google.maps.event.trigger(map,'resize');
        map.fitBounds(bounds);
      });
      if( _this.config.activeHole !== "" ) {
        var currentHole = _this.config.activeHole - 1;
        map.setCenter(new google.maps.LatLng(_this.config.wholesCenterLoc[currentHole][1],_this.config.wholesCenterLoc[currentHole][2]));
      } else if( _this.config.view ) {
        map.fitBounds(bounds);
      }

      /* Change markers on zoom */
      google.maps.event.addListener(map, 'zoom_changed', function() {
          var zoom = map.getZoom();
          // iterate over markers and call setVisible
          for (i = 0; i < courseMarkers.length; i++) {
              cMarkers[i].setVisible( (zoom >= 17) );
          }
      });



    },

    displayMessage: function() {
      alert(this.config.message);
    }
  }

  Plugin.defaults = Plugin.prototype.defaults;

  $.fn.echoMap = function(options) {
    return this.each(function() {
      new Plugin(this, options).init();
    });
  };

  window.Plugin = Plugin;

})(window, jQuery);


    // Docs at http://simpleweatherjs.com
    $(document).ready(function() {
      $.simpleWeather({
        location: '',
        woeid: '2391446',
        unit: 'f',
        success: function(weather) {
          html = '<div id="w-icon"><i class="wf wf-'+weather.code+'"></i></div><div id="temp-icon"> '+weather.temp+'&deg;<sub>'+weather.units.temp+'</sub></div>';
          html += '<div id="high-icon">High: <strong>'+weather.high+'&deg;</strong></div><div id="low-icon">Low: <strong>'+weather.low+'&deg;</strong></div>';
          html += '<div id="wind-icon">Wind: <strong>'+weather.wind.direction+' '+weather.wind.speed+' '+weather.units.speed+'</strong></div>';
          $("#weather").html(html);
        },
        error: function(error) {
          $("#weather").html('<p>'+error+'</p>');
        }
      });
    });