class AddFileFingerprintToMediaFiles < ActiveRecord::Migration
  def self.up
    add_column :media_files, :file_fingerprint, :string
  end

  def self.down
    remove_column :media_files, :file_fingerprint
  end
end
