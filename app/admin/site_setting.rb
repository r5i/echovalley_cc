ActiveAdmin.register SiteSetting do

  menu parent: 'Settings', url: '/admin/site_settings/1/edit'

  permit_params :address, :phone, :facebook_link, :twitter_link, :seo_text, :whatcounts_realm, :whatcounts_password

  actions :all, :except => [:show, :new, :destroy]

  controller do
    def edit
      @page_title = 'Edit Site Settings'
    end

    def update
      update! do |format|
        format.html { redirect_to edit_admin_site_setting_path(1), notice: 'Settings were successfully updated.' } if resource.valid?
      end
    end
  end

end