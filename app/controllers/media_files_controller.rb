class MediaFilesController < ApplicationController
  layout false

  def index
    @q = MediaFile.ransack(params[:q])
    @files = @q.result.order("updated_at desc").all.page params[:page]
  end

  def new
    @media_file = MediaFile.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @media_file }
    end
  end

  def create
    @media_file = MediaFile.new(media_file_params)
    if @media_file.save
      render json: @media_file, status: :ok
      # redirect_to media_files_path
    else
      render 'new'
    end
  end

  def edit
    @media_file = MediaFile.find(params[:id])
  end

  def update
    @media_file = MediaFile.find(params[:id])
  end

  def destroy
    @media_file = MediaFile.find(params[:id])
    @media_file.destroy
    redirect_to media_files_path
  end

  def delete_multiple
    @media_files = MediaFile.find(params[:media_file_ids]) unless params[:media_file_ids].blank?
    unless @media_files.nil?
      @media_files.each do |file|
        file.destroy
      end
    end
    redirect_to media_files_path
  end


  private

  def media_file_params
    params.fetch(:media_file, {}).permit(
        :file,
        :created_at,
        :updated_at,
        :file,
        :file_fingerprint,
        :fileable_id,
        :fileable_type)
  end

end