ActiveAdmin.register GolfCourse do

  menu priority: 1

  config.paginate = false
  config.batch_actions = false
  config.filters = false

  actions :all, :except => [:show, :destroy, :new]

  permit_params :name, :course_description, :slug,
                golf_holes_attributes: [ :id, :golf_course_id, :hole_number, :pro_tips, :par, :black_tee_yards, :white_tee_yards, :blue_tee_yards,
                :handicap, :gold_tee_yards, :gold_tee_par, :gold_tee_handicap, :black_lat, :black_long, :white_lat, :white_long,
                :blue_lat, :blue_long, :gold_lat, :gold_long, :hole_lat, :hole_long, golf_hole_photos_attributes: [ :id, :golf_hole_id, :photo, :photo_fingerprint, :created_at, :updated_at, :_destroy ]]

  controller do
    def edit
      @golf_course = GolfCourse.find(params[:id])
      @page_title = 'Edit - ' + @golf_course.name
    end
  end


  form do |f|
    f.inputs 'Course Details' do
      f.input :name, :label => 'Course Name:', :input_html => { :disabled => true }
      f.input :course_description, :label => 'Course Description:', :input_html => { :rows => 6 }
    end
    f.inputs 'Course Holes' do
      f.has_many :golf_holes, heading: 'Hole Details:', new_record: false do |h|
        h.input :hole_number, :input_html => { :disabled => true }
        h.input :pro_tips, :input_html => { :rows => 4 }
        h.input :par
        h.input :black_tee_yards
        h.input :black_lat
        h.input :black_long
        h.input :white_tee_yards
        h.input :white_lat
        h.input :white_long
        h.input :blue_tee_yards
        h.input :blue_lat
        h.input :blue_long
        h.input :handicap
        h.input :gold_tee_par
        h.input :gold_tee_yards
        h.input :gold_lat
        h.input :gold_long
        h.input :gold_tee_handicap
        h.input :hole_lat
        h.input :hole_long
        h.has_many :golf_hole_photos, heading: 'Golf Hole Photos', allow_destroy: true do |p|
          p.input :photo, :label => 'Golf Hole Photo:', :required => true, :as => :file, :hint => p.object.photo.present? ? image_tag(p.object.photo.url(:thumb), id: 'hole_thumb') : content_tag(:img, '',{ alt: "no image uploaded yet", id: 'hole_thumb'})
        end
      end
    end
    f.actions
  end

  index download_links: false, as: :block do |product|
    div for: product do
      # resource_selection_cell product
      h2  auto_link     link_to product.name, edit_admin_golf_course_path(product)
      div simple_format product.course_description
    end
  end



end