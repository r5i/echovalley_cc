# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160121152826) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_members", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_members", ["email"], name: "index_admin_members_on_email", unique: true, using: :btree
  add_index "admin_members", ["reset_password_token"], name: "index_admin_members_on_reset_password_token", unique: true, using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "banners", force: :cascade do |t|
    t.string   "name",                      limit: 255,                  null: false
    t.string   "headline",                  limit: 255,                  null: false
    t.text     "banner_text",               limit: 65535
    t.string   "link_to",                   limit: 255
    t.boolean  "active",                    limit: 1,     default: true
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "banner_image_file_name",    limit: 255
    t.string   "banner_image_content_type", limit: 255
    t.integer  "banner_image_file_size",    limit: 4
    t.datetime "banner_image_updated_at"
    t.string   "banner_image_fingerprint",  limit: 255
    t.integer  "position",                  limit: 4
  end

  create_table "course_statuses", force: :cascade do |t|
    t.text     "course_comment", limit: 65535
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "golf_course_photos", force: :cascade do |t|
    t.integer  "golf_course_id",     limit: 4,   null: false
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
    t.string   "photo_fingerprint",  limit: 255
  end

  create_table "golf_courses", force: :cascade do |t|
    t.string "name",               limit: 255,   null: false
    t.text   "course_description", limit: 65535
    t.string "slug",               limit: 255
  end

  create_table "golf_hole_photos", force: :cascade do |t|
    t.integer  "golf_hole_id",       limit: 4,   null: false
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
    t.string   "photo_fingerprint",  limit: 255
  end

  add_index "golf_hole_photos", ["golf_hole_id"], name: "fk_rails_4456ee51c6", using: :btree

  create_table "golf_holes", force: :cascade do |t|
    t.integer  "golf_course_id",    limit: 4
    t.integer  "hole_number",       limit: 4,                              null: false
    t.text     "pro_tips",          limit: 65535
    t.integer  "par",               limit: 4
    t.integer  "black_tee_yards",   limit: 4
    t.integer  "white_tee_yards",   limit: 4
    t.integer  "blue_tee_yards",    limit: 4
    t.string   "handicap",          limit: 255
    t.integer  "gold_tee_par",      limit: 4
    t.integer  "gold_tee_yards",    limit: 4
    t.string   "gold_tee_handicap", limit: 255
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.decimal  "black_lat",                       precision: 10, scale: 6
    t.decimal  "black_long",                      precision: 10, scale: 6
    t.decimal  "white_lat",                       precision: 10, scale: 6
    t.decimal  "white_long",                      precision: 10, scale: 6
    t.decimal  "blue_lat",                        precision: 10, scale: 6
    t.decimal  "blue_long",                       precision: 10, scale: 6
    t.decimal  "gold_lat",                        precision: 10, scale: 6
    t.decimal  "gold_long",                       precision: 10, scale: 6
    t.decimal  "hole_lat",                        precision: 10, scale: 6
    t.decimal  "hole_long",                       precision: 10, scale: 6
  end

  add_index "golf_holes", ["golf_course_id"], name: "index_golf_holes_on_golf_course_id", using: :btree

  create_table "media_files", force: :cascade do |t|
    t.integer  "fileable_id",       limit: 4
    t.string   "fileable_type",     limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.string   "file_fingerprint",  limit: 255
  end

  add_index "media_files", ["fileable_type", "fileable_id"], name: "index_media_files_on_fileable_type_and_fileable_id", using: :btree

  create_table "member_events", force: :cascade do |t|
    t.string   "event_title",   limit: 255
    t.text     "event_content", limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "news", force: :cascade do |t|
    t.string   "headline",   limit: 255
    t.text     "teaser",     limit: 65535
    t.text     "content",    limit: 65535
    t.boolean  "active",     limit: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "newsletter_campaigns", force: :cascade do |t|
    t.string   "name",                  limit: 255,   null: false
    t.text     "content",               limit: 65535
    t.string   "slug",                  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "layout_name",           limit: 255
    t.text     "custom_layout_content", limit: 65535
  end

  create_table "site_settings", force: :cascade do |t|
    t.string "address",             limit: 255
    t.string "phone",               limit: 255
    t.string "facebook_link",       limit: 255
    t.string "twitter_link",        limit: 255
    t.text   "seo_text",            limit: 65535
    t.string "whatcounts_realm",    limit: 255
    t.string "whatcounts_password", limit: 255
  end

  add_foreign_key "golf_hole_photos", "golf_holes"
end
