class CreateBanners < ActiveRecord::Migration
  def self.up
    create_table :banners do |t|
      t.string :name, null: false
      t.string :headline, null: false
      t.text :banner_text
      t.string :link_to
      t.boolean :active, default: true
      t.datetime :start_date
      t.datetime :end_date
      t.timestamps
    end
  end

  def self.down
    drop_table :banners
  end
end
