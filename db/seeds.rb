# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Adds default admin user for Active Admin
#AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

# Adds default admin member for Active Admin
#AdminMember.create!(email: 'member@example.com', password: 'password', password_confirmation: 'password')

# Create the site settings
SiteSetting.create!(address: '3150 Echo Valley Drive, Norwalk, IA 50211', phone: '515-285-0101',
                    seo_text: 'Located within 10 minutes of Downtown Des Moines and 10 minutes from the center of West Des Moines, Echo Valley Country Club embodies the spirit of its members... vibrant, friendly and expecting the very best in life. We are a private club promoting active lifestyles, social opportunities, family-centered recreation, and the very best of Midwest living')

GolfCourse.create!([{ name: 'The Creek', slug: 'thecreek'}, { name: 'The Vale', slug: 'thevale'}, { name: 'The Ridge', slug: 'theridge' }])

GolfHole.create!([{ golf_course_id: 2, hole_number: 1, par: 4, black_tee_yards: 386, black_lat: 41.511020, black_long: -93.661007, white_tee_yards: 334, white_lat: 41.510763, white_long: -93.661165,
                    blue_tee_yards: 302, blue_lat: 41.510524, blue_long: -93.661260, handicap: '16', gold_tee_par: 4, gold_tee_yards: 262, gold_lat: 41.510220, gold_long: -93.661275, gold_tee_handicap: '14', hole_lat: 41.508606, hole_long: -93.662988 },
                 { golf_course_id: 2, hole_number: 2, par: 4, black_tee_yards: 489, black_lat: 41.508454, black_long: -93.663624, white_tee_yards: 448, white_lat: 41.508480, white_long: -93.663825,
                   blue_tee_yards: 398, blue_lat: 41.508568, blue_long: -93.664705, handicap: '6', gold_tee_par: 5, gold_tee_yards: 393, gold_lat: 41.508558, gold_long: -93.665087, gold_tee_handicap: '6', hole_lat: 41.509001, hole_long: -93.668958 },
                 { golf_course_id: 2, hole_number: 3, par: 4, black_tee_yards: 375, black_lat: 41.509301, black_long: -93.669926, white_tee_yards: 345, white_lat: 41.509118, white_long: -93.670052,
                   blue_tee_yards: 312, blue_lat: 41.508861, blue_long: -93.670170, handicap: '8', gold_tee_par: 4, gold_tee_yards: 263, gold_lat: 41.508447, gold_long: -93.670164, gold_tee_handicap: '4', hole_lat: 41.506892, hole_long: -93.672157 },
                 { golf_course_id: 2, hole_number: 4, par: 4, black_tee_yards: 377, black_lat: 41.505915, black_long: -93.672409, white_tee_yards: 357, white_lat: 41.505915, white_long: -93.672409,
                   blue_tee_yards: 342, blue_lat: 41.505785, blue_long: -93.672406, handicap: '14', gold_tee_par: 4, gold_tee_yards: 286, gold_lat: 41.505357, gold_long: -93.672263, gold_tee_handicap: '12', hole_lat: 41.503305, hole_long: -93.671199},
                 { golf_course_id: 2, hole_number: 5, par: 3, black_tee_yards: 177, black_lat: 41.502937, black_long: -93.671127, white_tee_yards: 148, white_lat: 41.502793, white_long: -93.671093,
                   blue_tee_yards: 140, blue_lat: 41.502700, blue_long: -93.671054, handicap: '18', gold_tee_par: 3, gold_tee_yards: 133, gold_lat: 41.502642, gold_long: -93.671038, gold_tee_handicap: '16', hole_lat: 41.501570, hole_long: -93.670779 },
                 { golf_course_id: 2, hole_number: 6, par: 5, black_tee_yards: 600, black_lat: 41.501361, black_long: -93.670039, white_tee_yards: 552, white_lat: 41.501761, white_long: -93.669988,
                   blue_tee_yards: 494, blue_lat: 41.502123, blue_long: -93.670224, handicap: '4', gold_tee_par: 5, gold_tee_yards: 397, gold_lat: 41.502990, gold_long: -93.670439, gold_tee_handicap: '2', hole_lat: 41.506179, hole_long: -93.669718 },
                 { golf_course_id: 2, hole_number: 7, par: 3, black_tee_yards: 238, black_lat: 41.507135, black_long: -93.670393, white_tee_yards: 190, white_lat: 41.507135, white_long: -93.670393,
                   blue_tee_yards: 143, blue_lat: 41.507521, blue_long: -93.670019, handicap: '10', gold_tee_par: 3, gold_tee_yards: 112, gold_lat: 41.507744, gold_long: -93.669895, gold_tee_handicap: '18', hole_lat: 41.508759, hole_long: -93.669597 },
                 { golf_course_id: 2, hole_number: 8, par: 4, black_tee_yards: 448, black_lat: 41.509447, black_long: -93.669150, white_tee_yards: 434, white_lat: 41.509459, white_long: -93.669002,
                   blue_tee_yards: 383, blue_lat: 41.509596, blue_long: -93.668491, handicap: '2', gold_tee_par: 4, gold_tee_yards: 287, gold_lat: 41.509842, gold_long: -93.667431, gold_tee_handicap: '10', hole_lat: 41.509205, hole_long: -93.664390 },
                 { golf_course_id: 2, hole_number: 9, par: 4, black_tee_yards: 357, black_lat: 41.508769, black_long: -93.663649, white_tee_yards: 337, white_lat: 41.508918, white_long: -93.663556,
                   blue_tee_yards: 327, blue_lat: 41.508918, blue_long: -93.663556, handicap: '12', gold_tee_par: 4, gold_tee_yards: 244, gold_lat: 41.509612, gold_long: -93.663279, gold_tee_handicap: '8', hole_lat: 41.511135, hole_long: -93.661471 },
                 { golf_course_id: 1, hole_number: 1, par: 4, black_tee_yards: 377, black_lat: 41.510973, black_long: -93.659819, white_tee_yards: 332, white_lat: 41.510650, white_long: -93.659693,
                   blue_tee_yards: 327, blue_lat: 41.510546, blue_long: -93.659656, handicap: '11/12', gold_tee_par: 4, gold_tee_yards: 262, gold_lat: 41.510461, gold_long: -93.659628, gold_tee_handicap: '13/14', hole_lat: 41.507927, hole_long: -93.658859 },
                 { golf_course_id: 1, hole_number: 2, par: 5, black_tee_yards: 532, black_lat: 41.507630, black_long: -93.657523, white_tee_yards: 517, white_lat: 41.507664, white_long: -93.657460,
                   blue_tee_yards: 475, blue_lat: 41.507846, blue_long: -93.657164, handicap: '7/8', gold_tee_par: 5, gold_tee_yards: 404, gold_lat: 41.508174, gold_long: -93.656481, gold_tee_handicap: '1/2', hole_lat: 41.507974, hole_long: -93.652368 },
                 { golf_course_id: 1, hole_number: 3, par: 3, black_tee_yards: 179, black_lat: 41.507532, black_long: -93.652280, white_tee_yards: 169, white_lat: 41.507532, white_long: -93.652280,
                   blue_tee_yards: 159, blue_lat: 41.507543, blue_long: -93.651632, handicap: '17/18', gold_tee_par: 3, gold_tee_yards: 119, gold_lat: 41.507543, gold_long: -93.651632, gold_tee_handicap: '17/18', hole_lat: 41.506941, hole_long: -93.650555 },
                 { golf_course_id: 1, hole_number: 4, par: 4, black_tee_yards: 342, black_lat: 41.506210, black_long: -93.649928, white_tee_yards: 302, white_lat: 41.506435, white_long: -93.650145,
                   blue_tee_yards: 292, blue_lat: 41.506447, blue_long: -93.650226, handicap: '15/16', gold_tee_par: 4, gold_tee_yards: 279, gold_lat: 41.506455, gold_long: -93.650295, gold_tee_handicap: '9/10', hole_lat: 41.507444, hole_long: -93.653143 },
                 { golf_course_id: 1, hole_number: 5, par: 4, black_tee_yards: 399, black_lat: 41.507444, black_long: -93.653665, white_tee_yards: 372, white_lat: 41.507135, white_long: -93.653707,
                   blue_tee_yards: 364, blue_lat: 41.507135, blue_long: -93.653707, handicap: '3/4', gold_tee_par: 4, gold_tee_yards: 317, gold_lat: 41.506754, gold_long: -93.654252, gold_tee_handicap: '3/4', hole_lat: 41.505156, hole_long: -93.656779 },
                 { golf_course_id: 1, hole_number: 6, par: 5, black_tee_yards: 533, black_lat: 41.504927, black_long: -93.658272, white_tee_yards: 490, white_lat: 41.505039, white_long: -93.657779,
                   blue_tee_yards: 445, blue_lat: 41.505297, blue_long: -93.657576, handicap: '9/10', gold_tee_par: 5, gold_tee_yards: 366, gold_lat: 41.505901, gold_long: -93.657482, gold_tee_handicap: '5/6', hole_lat: 41.507779, hole_long: -93.654157 },
                 { golf_course_id: 1, hole_number: 7, par: 4, black_tee_yards: 455, black_lat: 41.507673, black_long: -93.655391, white_tee_yards: 440, white_lat: 41.507673, white_long: -93.655391,
                   blue_tee_yards: 419, blue_lat: 41.507638, blue_long: -93.655761, handicap: '1/2', gold_tee_par: 4, gold_tee_yards: 240, gold_lat: 41.507336, gold_long: -93.657459, gold_tee_handicap: '11/12', hole_lat: 41.505432, hole_long: -93.658636 },
                 { golf_course_id: 1, hole_number: 8, par: 3, black_tee_yards: 192, black_lat: 41.505699, black_long: -93.659157, white_tee_yards: 177, white_lat: 41.505761, white_long: -93.659128,
                   blue_tee_yards: 165, blue_lat: 41.506234, blue_long: -93.658718, handicap: '13/14', gold_tee_par: 3, gold_tee_yards: 120, gold_lat: 41.506366, gold_long: -93.658715, gold_tee_handicap: '15/16', hole_lat: 41.507304, hole_long: -93.658866 },
                 { golf_course_id: 1, hole_number: 9, par: 4, black_tee_yards: 427, black_lat: 41.507587, black_long: -93.658103, white_tee_yards: 407, white_lat: 41.507691, white_long: -93.658114,
                   blue_tee_yards: 362, blue_lat: 41.508079, blue_long: -93.658229, handicap: '5/6', gold_tee_par: 4, gold_tee_yards: 266, gold_lat: 41.508875, gold_long: -93.658130, gold_tee_handicap: '7/8', hole_lat: 41.511003, hole_long: -93.659120 },
                 { golf_course_id: 3, hole_number: 1, par: 4, black_tee_yards: 394, black_lat: 41.504586, black_long: -93.659264, white_tee_yards: 362, white_lat: 41.504478, white_long: -93.658913,
                   blue_tee_yards: 319, blue_lat: 41.504321, blue_long: -93.658340, handicap: '3', gold_tee_par: 4, gold_tee_yards: 255, gold_lat: 41.504327, gold_long: -93.657758, gold_tee_handicap: '11', hole_lat: 41.505012, hole_long: -93.655234 },
                 { golf_course_id: 3, hole_number: 2, par: 4, black_tee_yards: 417, black_lat: 41.504335, black_long: -93.655013, white_tee_yards: 393, white_lat: 41.503775, white_long: -93.655187,
                   blue_tee_yards: 356, blue_lat: 41.503361, blue_long: -93.655305, handicap: '11', gold_tee_par: 4, gold_tee_yards: 304, gold_lat: 41.503070, gold_long: -93.655464, gold_tee_handicap: '1', hole_lat: 41.501940, hole_long: -93.657716 },
                 { golf_course_id: 3, hole_number: 3, par: 3, black_tee_yards: 176, black_lat: 41.501372, black_long: -93.657976, white_tee_yards: 157, white_lat: 41.501310, white_long: -93.657559,
                   blue_tee_yards: 148, blue_lat: 41.501306, blue_long: -93.657363, handicap: '17', gold_tee_par: 3, gold_tee_yards: 129, gold_lat: 41.501312, gold_long: -93.657283, gold_tee_handicap: '15', hole_lat: 41.501547, hole_long: -93.655872 },
                 { golf_course_id: 3, hole_number: 4, par: 5, black_tee_yards: 580, black_lat: 41.500484, black_long: -93.655692, white_tee_yards: 538, white_lat: 41.500400, white_long: -93.656151,
                   blue_tee_yards: 479, blue_lat: 41.500439, blue_long: -93.656701, handicap: '1', gold_tee_par: 5, gold_tee_yards: 429, gold_lat: 41.500419, gold_long: -93.657190, gold_tee_handicap: '3', hole_lat: 41.500700, hole_long: -93.661497 },
                 { golf_course_id: 3, hole_number: 5, par: 4, black_tee_yards: 458, black_lat: 41.501263, black_long: -93.664309, white_tee_yards: 429, white_lat: 41.501107, white_long: -93.664472,
                   blue_tee_yards: 377, blue_lat: 41.501406, blue_long: -93.664969, handicap: '5', gold_tee_par: 4, gold_tee_yards: 336, gold_lat: 41.501437, gold_long: -93.665381, gold_tee_handicap: '7', hole_lat: 41.501286, hole_long: -93.669197 },
                 { golf_course_id: 3, hole_number: 6, par: 3, black_tee_yards: 204, black_lat: 41.500663, black_long: -93.669464, white_tee_yards: 188, white_lat: 41.500396, white_long: -93.669384,
                   blue_tee_yards: 160, blue_lat: 41.500185, blue_long: -93.669476, handicap: '9', gold_tee_par: 3, gold_tee_yards: 135, gold_lat: 41.499992, gold_long: -93.669585, gold_tee_handicap: '17', hole_lat: 41.498966, hole_long: -93.670329 },
                 { golf_course_id: 3, hole_number: 7, par: 4, black_tee_yards: 406, black_lat: 41.497641, black_long: -93.671064, white_tee_yards: 361, white_lat: 41.497465, white_long: -93.670634,
                   blue_tee_yards: 306, blue_lat: 41.497424, blue_long: -93.669915, handicap: '15', gold_tee_par: 4, gold_tee_yards: 258, gold_lat: 41.497402, gold_long: -93.669355, gold_tee_handicap: '13', hole_lat: 41.497917, hole_long: -93.666821 },
                 { golf_course_id: 3, hole_number: 8, par: 4, black_tee_yards: 418, black_lat: 41.497561, black_long: -93.666266, white_tee_yards: 365, white_lat: 41.497860, white_long: -93.665870,
                   blue_tee_yards: 317, blue_lat: 41.498100, blue_long: -93.665473, handicap: '7', gold_tee_par: 4, gold_tee_yards: 272, gold_lat: 41.498393, gold_long: -93.665029, gold_tee_handicap: '9', hole_lat: 41.500506, hole_long: -93.663915 },
                 { golf_course_id: 3, hole_number: 9, par: 5, black_tee_yards: 551, black_lat: 41.501351, black_long: -93.663451, white_tee_yards: 524, white_lat: 41.501611, white_long: -93.663488,
                   blue_tee_yards: 497, blue_lat: 41.502210, blue_long: -93.663336, handicap: '13', gold_tee_par: 5, gold_tee_yards: 455, gold_lat: 41.502516, gold_long: -93.663204, gold_tee_handicap: '5', hole_lat: 41.505158, hole_long: -93.660235}
                 ])
