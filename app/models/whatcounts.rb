class Whatcounts

  require 'uri'

  def self.show_list(wc_params)
    response = HTTParty.get('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&output_format=xml&c=show_lists')
    data = Hash.from_xml(response.body)
    return data
  end

  def self.get_list(wc_params, list_id)
    response = HTTParty.get('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&output_format=xml&c=getlistbyid&list_id=' + list_id + '')
    data = Hash.from_xml(response.body)
    return data
  end

  def self.show_templates(wc_params)
    response = HTTParty.get('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&output_format=xml&c=show_templates')
    data = Hash.from_xml(response.body)
    return data
  end

  def self.get_template(wc_params, template_id)
    response = HTTParty.get('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&output_format=xml&c=gettemplatebyid&template_id=' + template_id + '')
    if response.body.include?("<Data>")
      data = Hash.from_xml(response.body)
    else
      data = nil
    end
    return data
  end

  def self.template_preview(wc_params, template_name)
    response = HTTParty.get('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&c=templatepreview&template_name=' + template_name + '&template_type=2')
    return response.body
  end

  def self.create_template(wc_params, data = {})
    HTTParty.post('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&c=createtemplate&template_name=' + data[:template_name] + '&template_subject=' + data[:template_subject] + '&template_body_html=' + URI.escape(data[:template_html]) + '')
  end

  def self.update_template(wc_params, template_name, data = {})
    data[:template_subject].blank? ? subject = "&template_subject=No Subject" : subject = "&template_subject=#{data[:template_subject]}"
    HTTParty.post('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&c=updatetemplate&template_name=' + template_name + '&template_body_html=' + URI.escape(data[:template_html]) + '' + subject + '&charset=[utf-8]&encoding=quoted')
  end

  def self.send_test_newsletter(wc_params, list_id, data = {})
    data[:subject].blank? ? subject = "" : subject = "&subject=#{data[:subject]}"
    data[:email].blank? ? email = "" : email = "&to=#{data[:email]}"
    HTTParty.post('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&c=send&format=2&list_id=' + list_id + '&template_id=' + data[:template] + '' + subject + '' + email)
  end

  def self.send_campaign_for_approval(wc_params, list_id, data = {})
    data[:subject].blank? ? subject = "" : subject = "&subject=#{data[:subject]}"
    HTTParty.post('http://whatcounts.com/bin/api_web?r=' + wc_params[:realm] + '&p=' + wc_params[:pass] + '&c=launchwithcount&format=2&list_id=' + list_id + '&template_id=' + data[:template] + '' + subject + '')
  end
end