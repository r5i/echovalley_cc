ActiveAdmin.register CourseStatus do
   menu label: "Course Status"
   
   permit_params :course_comment
   
   form do |f|
    f.inputs do
      f.input :course_comment, as: :html_editor
    end

    f.actions
  end

end