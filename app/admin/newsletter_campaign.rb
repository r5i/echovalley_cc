ActiveAdmin.register NewsletterCampaign do

  permit_params :name, :content, :slug, :layout_name, :custom_layout_content, :created_at, :updated_at

  menu :parent => "Newsletters"

  config.sort_order = 'updated_at_desc'
  config.per_page = 30

  actions :all, :except => :show

  index download_links: false do
    selectable_column
    column :name, :sortable => :name do |page|
      link_to page.name, edit_admin_newsletter_campaign_path(page)
    end

    column :updated_at
    column :created_at

    actions
  end

  form :partial => "newsletter_campaign_form"

end