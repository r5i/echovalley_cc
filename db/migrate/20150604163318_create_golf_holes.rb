class CreateGolfHoles < ActiveRecord::Migration
  def self.up
    create_table :golf_holes do |t|
      t.belongs_to :golf_course, index:true
      t.integer    :hole_number, null: false
      t.text       :pro_tips
      t.integer    :par
      t.integer    :black_tee_yards
      t.integer    :white_tee_yards
      t.integer    :blue_tee_yards
      t.string    :handicap
      t.integer    :gold_tee_par
      t.integer    :gold_tee_yards
      t.string    :gold_tee_handicap
      t.timestamps null: false
    end
  end

  def self.down
    drop_table :golf_holes
  end
end
