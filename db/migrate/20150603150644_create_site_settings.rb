class CreateSiteSettings < ActiveRecord::Migration
  def self.up
    create_table :site_settings do |t|
      t.string :address
      t.string :phone
      t.string :facebook_link
      t.string :twitter_link
      t.text   :seo_text

    end
  end

  def self.down
    drop_table :site_settings
  end
end
