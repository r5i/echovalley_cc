# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'echovalley'
# set :repo_url, 'https://stmifaust1:PayLink0806@bitbucket.org/r5i/echovalley.git'
set :repo_url, 'git@bitbucket.org:r5i/echovalley.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/sites/echovalley'

# Default value for :scm is :git
set :scm, :git


set :rvm_type, :system


# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'app/views/layouts/newsletters')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

set :migrate_target, :latest

# Restart using 'passenger-config restart-app'
set :passenger_restart_with_touch, false

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
