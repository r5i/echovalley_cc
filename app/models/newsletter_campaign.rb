class NewsletterCampaign < ActiveRecord::Base
  liquid_methods :golf_courses

  before_save :create_slug, if: :new_record?

  validates :name, :content, presence: true

  def create_slug
    self.slug = name.parameterize
  end

  def golf_courses
    GolfCourse.all
  end


end