class MembersSectionController < ApplicationController
  def index
  end
  
   def course_guide
  end

  def directory
  end

  def member_events
  end

  def member_home
  end

  def news_article
  end

  def pro_shop
  end

  def recent_events
  end

  def results
  end
end
