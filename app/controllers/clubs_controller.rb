class ClubsController < ApplicationController
  protect_from_forgery

  def index
  end

  def recreation
  end

  def events
  end

  def guest_information
  end

  def community
  end

  def menu
  end

  def dress_code
  end

  def history
  end

  def careers
  end

  def directions
  end

  def contact
  end

end