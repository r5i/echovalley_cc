class NewsletterCampaignsController < ApplicationController
  protect_from_forgery
  before_action :find_newsletter, only: :show


  def show

  end


  private

  def find_newsletter
    @newsletter = NewsletterCampaign.find_by(slug: params[:slug])
    if @newsletter.layout_name == ""
      self.class.layout(false)
    else
      self.class.layout(@newsletter.layout_name)
    end
  end


end